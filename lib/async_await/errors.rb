module AsyncAwait
  class Error < StandardError ; end

  class AlreadyCompletedError < Error
    def initialize(result, error)
      if result
        super("Result is already set: #{result}")
      elsif error
        super("Error is already set: #{error}")
      else
        ArgumentError.new('Either result or error must be set')
      end
    end
  end
end
