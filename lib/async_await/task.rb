module AsyncAwait
  class Task
    def self.create(&block)
      Class.new(Task) do |klass|
        klass.define_method(:run, &block)
      end.new
    end

    def initialize
      @completed = false
    end

    private def resolve(result = nil)
      raise AlreadyCompletedError.new(@result, @error) if @completed
      @completed = true
      @result = result
      @on_result&.call(result)
    end

    private def reject(error)
      raise AlreadyCompletedError.new(@result, @error) if @completed
      @completed = true
      @error = error
      @on_error&.call(error)
    end

    private def completed?
      @completed
    end

    def then(another_task = nil, &block)
      AsyncAwait::TaskCallbackChain.new(self).then(another_task, &block)
    end

    def run
      throw NotImplementedError.new('#run should be overriden')
    end

    def execute(*args)
      call_result = run(*args)
    rescue => err
      if !@completed
        reject(err)
      end
    end

    def on_result(&block)
      @on_result = block
    end

    def on_error(&block)
      @on_error = block
    end

    attr_reader :result, :error
  end

  class AutoResolvingTask < Task
    def initialize(&block)
      super
      @proc = block
    end
    # @override
    def execute(*args)
      call_result = @proc.call(*args)
      if !@completed
        resolve(call_result)
      end
    rescue => err
      if !@completed
        reject(err)
      end
    end
  end
end
