module AsyncAwait
  class TaskCallbackChain
    def initialize(*task_args)
      @tasks = task_args
    end

    def then(another_task = nil, &block)
      task =
        if block
          AsyncAwait::AutoResolvingTask.new(&block)
        else
          another_task
        end

      return self if task.nil?

      tasks = @tasks.dup
      tasks << task
      AsyncAwait::TaskCallbackChain.new(*tasks)
    end

    def call(*args)
      AsyncAwait::TaskRunner.run(@tasks, *args)
    end
  end
end
