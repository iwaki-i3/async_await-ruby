module AsyncAwait
  class TaskRunner
    def self.run(tasks, *args)
      unless tasks.empty?
        AsyncAwait::TaskRunner.new(tasks.first, tasks[1..], args).run
      end
    end

    def initialize(task, next_tasks, args = [])
      @task = task
      @next_tasks = next_tasks
      @args = args
    end

    def run
      @task.on_result do |result|
        @result = result
        unless @next_tasks.empty?
          AsyncAwait::TaskRunner.new(@next_tasks.first, @next_tasks[1..], [result]).run
        end
      end
      @task.on_error do |error|
        @error = error
      end

      @task.execute(*@args)
    end
  end
end
