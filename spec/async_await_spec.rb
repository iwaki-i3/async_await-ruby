RSpec.describe AsyncAwait do
  describe 'async/await' do
    let(:timed_results) { [] }
    let(:start_time) { Time.now }

    TimedResult = Struct.new(:event, :timestamp, keyword_init: true)

    subject {
      async def after(timeout_ms)
        sleep(timeout_ms / 1000.0)
      end

      start_time
      run_with_blocking {
        task = async do
          await after(3000)

          timed_results << TimedResult.new(event: "after_3000", timestamp: Time.now - start_time)

          await after(2000)

          timed_results << TimedResult.new(event: "after_2000", timestamp: Time.now - start_time)
        end
        timed_results << TimedResult.new(event: "after_async_window", timestamp: Time.now - start_time)
        await task
        timed_results << TimedResult.new(event: "end_of_run_with_blocking", timestamp: Time.now - start_time)
      }
      timed_results << TimedResult.new(event: "after_run_with_blocking", timestamp: Time.now - start_time)
    }

    it {
      subject
      expect(timed_results[0].event).to 'after_async_window'
      expect(Time.now - timed_results[0].timestamp).to be_between(0, 0.2)
      expect(timed_results[1].event).to 'after_3000'
      expect(Time.now - timed_results[0].timestamp).to be_between(2.8, 3.2)
      expect(timed_results[1].event).to 'after_2000'
      expect(Time.now - timed_results[0].timestamp).to be_between(2.8, 3.2)
    }
  end
end
